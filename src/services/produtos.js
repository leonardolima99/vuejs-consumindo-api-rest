const { api } = require('./api')

export default {
	listar: (page = 1, limit = 10) => {
		return api.get(`/products?page=${page}&limit=${limit}`)
	},
	mostrar: (id) => {
		return api.get('/products/' + id)
	},
	criar: (produto) => {
		return api.post('/products', produto)
	},
	editar: (id, produto) => {
		return api.put('/products/' + id, produto)
	},
	deletar: (id) => {
		return api.delete('/products/' + id)
	}
}