import axios from 'axios'

export const api = axios.create({
	baseURL: 'https://apirestjs.herokuapp.com/api'
	// baseURL: 'http://localhost:3001/api'
})
